Rails.application.routes.draw do
  get 'tests/show_results'
  post 'tests/show_results'
  get 'tests/finish_test'
  post 'tests/finish_test'
  resources :tests
  get 'orders/add_discount'
  post 'orders/add_discount'
  get 'orders/pay_order'
  post 'orders/pay_order'
  get 'orders/print_order'
  post 'orders/print_order'
  resources :orders
  get 'patients/search'
  post 'patients/search'
  get 'patients/add_patient_type'
  post 'patients/add_patient_type'
  get 'patients/remove_patient_type'
  post 'patients/remove_patient_type'
  resources :patients
  resources :patient_types
  resources :field_types
  get 'medteches' => 'medteches#index', :as => 'personnels'
  resources :medteches
  get 'test_types/search'
  post 'test_types/search'
  resources :test_types
  resources :test_fields
  resources :physicians
  devise_for :users, controllers:{
    sessions: 'users/sessions',
    registrations:  'users/registrations',
    passwords: 'users/passwords'
  }
  get 'users/create_user'
  post 'users/create_user'
  resources :users
  resources :roles
  get 'static/index'

  root to: 'static#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
