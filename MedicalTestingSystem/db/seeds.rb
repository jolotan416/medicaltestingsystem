# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
admin_role = Role.create(name: "Administrator", description: "Has all access to the whole website")
employee_role = Role.create(name: "Employee", description: "Can create/edit/view/delete objects")

user1 = User.create(username: 'sampleadmin', password: 'adminpass', first_name: 'Admin', middle_name: 'Admin', last_name: 'Admin', role_id: 1, mobile_number: "09177098787", email: "jolotan416@gmail.com")
user2 = User.create(username: 'sampleuser', password: 'userpass', first_name: 'Employee', middle_name: 'Employee', last_name: 'Employee', role_id: 2, mobile_number: "09171234567", email: "employee@gmail.com")

line1 = Line.create(name: 'Lab', user_id: 1)
line2 = Line.create(name: 'X-ray', user_id: 2)
line3 = Line.create(name: 'Consultation', user_id: 2)

medtech1 = Medtech.create(first_name: 'Medtech', middle_name: 'Medtech', last_name: 'Medtech', user_id: 1)
medtech2 = Medtech.create(first_name: 'Blood', middle_name: 'Medtech', last_name: 'Medtech', user_id: 2)

physician1 = Physician.create(first_name: 'John', middle_name: 'Matthew', last_name: 'Smith', prefix: 'Doctor', suffix: 'MD', user_id: 1)
physician2 = Physician.create(first_name: 'John', middle_name: 'Haymish', last_name: 'Watson', user_id: 2)

test_type1 = TestType.create(name: 'FBS', amount: 500, line_id: 1, user_id: 1)
test_type2 = TestType.create(name: 'Consultation', amount: 500, physician_id: 1, line_id: 3, user_id: 2)
test_type3 = TestType.create(name: 'Consultation', amount: 1000, physician_id: 2, line_id: 3, user_id: 2)

field_type1 = FieldType.create(name: 'Red Blood Cells', user_id: 1)
field_type2 = FieldType.create(name: 'White Blood Cells', user_id: 1)
field_type3 = FieldType.create(name: 'Platelets', user_id: 1)
field_type4 = FieldType.create(name: 'Remarks', user_id: 2)

patient1 = Patient.create(first_name: 'Jacqueline Mae', middle_name: 'Limsui', last_name: 'Tan', sex: 'Female', birthday: Date.strptime("10/17/1995", "%m/%d/%Y"), status: 'Single', address: '124 B Serrano St., Caloocan City', contact_no: '09178797449', user_id: 1)
patient2 = Patient.create(first_name: 'Jazmin Megan', middle_name: 'Limsui', last_name: 'Tan', sex: 'Female', birthday: Date.strptime("05/03/1992", "%m/%d/%Y"), status: 'Single', address: '124 B Serrano St., Caloocan City', contact_no: '09178797449', user_id: 2)
patient3 = Patient.create(first_name: 'Veneracion', middle_name: 'Arellano', last_name: 'Hinayon', sex: 'Female', birthday: Date.strptime("09/16/1950", "%m/%d/%Y"), status: 'Married', address: '146 H4 N. Domingo St., Cubao, Quezon City', contact_no: '4165373', user_id: 1)

patient_type1 = PatientType.create(name: 'Philhealth Cardholder', discount: '0.1', user_id: 1)
patient_type2 = PatientType.create(name: 'Senior Citizen', discount: '0.2', user_id: 1)

patient_type_lookup1 = PatientTypeLookup.create(patient_id: 1, patient_type_id: 1, user_id: 1)
patient_type_lookup2 = PatientTypeLookup.create(patient_id: 3, patient_type_id: 1, user_id: 1)
patient_type_lookup3 = PatientTypeLookup.create(patient_id: 3, patient_type_id: 2, user_id: 1)

order1 = Order.create(date_ordered: DateTime.strptime("03/17/2017 08:00", "%m/%d/%Y %H:%M"), subtotal: 1000, discount: 0.2, total: 800, payment: 1000, change: 200, patient_id: 3, user_id: 1)
order2 = Order.create(date_ordered: DateTime.strptime("03/17/2017 10:00", "%m/%d/%Y %H:%M"), subtotal: 1000, discount: 0, total: 1000, payment: 1000, change: 0, patient_id: 2, user_id: 2)

test1 = Test.create(date_received: DateTime.strptime("03/19/2017 08:00", "%m/%d/%Y %H:%M"), test_type_id: 1, medtech_id: 1, order_id: 1, user_id: 1)
test2 = Test.create(date_received: DateTime.strptime("03/19/2017 08:00", "%m/%d/%Y %H:%M"), test_type_id: 2, order_id: 1, user_id: 1)
test3 = Test.create(test_type_id: 3, order_id: 2, user_id: 2)

test_field1 = TestField.create(test_id: 1, data: "5", field_type_id: 1, user_id: 1)
test_field1 = TestField.create(test_id: 1, data: "10", field_type_id: 2, user_id: 1)
test_field1 = TestField.create(test_id: 1, data: "20", field_type_id: 3, user_id: 1)
test_field1 = TestField.create(test_id: 1, data: "Normal", field_type_id: 4, user_id: 1)
test_field1 = TestField.create(test_id: 2, data: "Patient does not have any ailments", field_type_id: 4, user_id: 1)
test_field1 = TestField.create(test_id: 3, data: "Patient has cancer. Ohno.", field_type_id: 4, user_id: 2)
