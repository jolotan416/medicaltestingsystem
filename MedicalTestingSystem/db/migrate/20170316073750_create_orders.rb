class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.datetime :date_ordered, null: false, default: -> {'CURRENT_TIMESTAMP'}
      t.float :subtotal, null: false, default: 0
      t.float :discount, null: false, default: 0
      t.float :total, null: false, default: 0
      t.float :payment, null: false, default: 0
      t.float :change, null: false, default: 0
      t.references :patient, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
