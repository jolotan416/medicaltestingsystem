class CreatePatients < ActiveRecord::Migration[5.0]
  def change
    create_table :patients do |t|
      t.string :first_name, null: false, default: ""
      t.string :middle_name, null: false, default: ""
      t.string :last_name, null: false, default: ""
      t.string :sex, null: false, default: ""
      t.date :birthday, null: false
      t.string :status, null: false, default: ""
      t.string :address, null: false, default: ""
      t.bigint :contact_no, null: false, default: "09171234567"
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
