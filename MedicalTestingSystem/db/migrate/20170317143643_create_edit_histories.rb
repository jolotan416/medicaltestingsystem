class CreateEditHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :edit_histories do |t|
      t.string :table_name
      t.integer :table_id
      t.datetime :date_edited
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
