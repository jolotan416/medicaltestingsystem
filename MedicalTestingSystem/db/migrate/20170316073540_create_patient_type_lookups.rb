class CreatePatientTypeLookups < ActiveRecord::Migration[5.0]
  def change
    create_table :patient_type_lookups do |t|
      t.references :patient, foreign_key: true
      t.references :patient_type, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
