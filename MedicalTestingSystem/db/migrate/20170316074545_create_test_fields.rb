class CreateTestFields < ActiveRecord::Migration[5.0]
  def change
    create_table :test_fields do |t|
      t.references :test, foreign_key: true
      t.text :data
      t.references :field_type, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
