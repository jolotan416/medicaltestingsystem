class CreatePhysicians < ActiveRecord::Migration[5.0]
  def change
    create_table :physicians do |t|
      t.string :first_name, null: false, default: ""
      t.string :middle_name, null: false, default: ""
      t.string :last_name, null: false, default: ""
      t.string :prefix
      t.string :suffix
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
