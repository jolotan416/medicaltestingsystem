class CreateTests < ActiveRecord::Migration[5.0]
  def change
    create_table :tests do |t|
      t.datetime :date_received
      t.datetime :date_done
      t.references :test_type, foreign_key: true
      t.references :medtech, foreign_key: true
      t.references :order, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
