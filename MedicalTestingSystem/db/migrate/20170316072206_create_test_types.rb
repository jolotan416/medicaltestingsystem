class CreateTestTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :test_types do |t|
      t.string :name, null: false, default: ""
      t.float :amount, null: false, default: 0
      t.references :physician, foreign_key: true
      t.references :line, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
