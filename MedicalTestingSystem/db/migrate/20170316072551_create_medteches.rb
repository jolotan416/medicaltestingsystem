class CreateMedteches < ActiveRecord::Migration[5.0]
  def change
    create_table :medteches do |t|
      t.string :first_name, null: false, default: ""
      t.string :middle_name, null: false, default: ""
      t.string :last_name, null: false, default: ""
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
