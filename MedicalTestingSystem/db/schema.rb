# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170317143643) do

  create_table "edit_histories", force: :cascade do |t|
    t.string   "table_name"
    t.integer  "table_id"
    t.datetime "date_edited"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_edit_histories_on_user_id"
  end

  create_table "field_types", force: :cascade do |t|
    t.string   "name",       default: "", null: false
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_field_types_on_user_id"
  end

  create_table "lines", force: :cascade do |t|
    t.string   "name",       default: "", null: false
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_lines_on_user_id"
  end

  create_table "medteches", force: :cascade do |t|
    t.string   "first_name",  default: "", null: false
    t.string   "middle_name", default: "", null: false
    t.string   "last_name",   default: "", null: false
    t.integer  "user_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id"], name: "index_medteches_on_user_id"
  end

  create_table "orders", force: :cascade do |t|
    t.datetime "date_ordered",               null: false
    t.float    "subtotal",     default: 0.0, null: false
    t.float    "discount",     default: 0.0, null: false
    t.float    "total",        default: 0.0, null: false
    t.float    "payment",      default: 0.0, null: false
    t.float    "change",       default: 0.0, null: false
    t.integer  "patient_id"
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["patient_id"], name: "index_orders_on_patient_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "patient_type_lookups", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "patient_type_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["patient_id"], name: "index_patient_type_lookups_on_patient_id"
    t.index ["patient_type_id"], name: "index_patient_type_lookups_on_patient_type_id"
    t.index ["user_id"], name: "index_patient_type_lookups_on_user_id"
  end

  create_table "patient_types", force: :cascade do |t|
    t.string   "name",       default: "", null: false
    t.float    "discount"
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_patient_types_on_user_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string   "first_name",  default: "",         null: false
    t.string   "middle_name", default: "",         null: false
    t.string   "last_name",   default: "",         null: false
    t.string   "sex",         default: "",         null: false
    t.date     "birthday",                         null: false
    t.string   "status",      default: "",         null: false
    t.string   "address",     default: "",         null: false
    t.bigint   "contact_no",  default: 9171234567, null: false
    t.integer  "user_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["user_id"], name: "index_patients_on_user_id"
  end

  create_table "physicians", force: :cascade do |t|
    t.string   "first_name",  default: "", null: false
    t.string   "middle_name", default: "", null: false
    t.string   "last_name",   default: "", null: false
    t.string   "prefix"
    t.string   "suffix"
    t.integer  "user_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id"], name: "index_physicians_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",        default: "", null: false
    t.text     "description"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "test_fields", force: :cascade do |t|
    t.integer  "test_id"
    t.text     "data"
    t.integer  "field_type_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["field_type_id"], name: "index_test_fields_on_field_type_id"
    t.index ["test_id"], name: "index_test_fields_on_test_id"
    t.index ["user_id"], name: "index_test_fields_on_user_id"
  end

  create_table "test_types", force: :cascade do |t|
    t.string   "name",         default: "",  null: false
    t.float    "amount",       default: 0.0, null: false
    t.integer  "physician_id"
    t.integer  "line_id"
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["line_id"], name: "index_test_types_on_line_id"
    t.index ["physician_id"], name: "index_test_types_on_physician_id"
    t.index ["user_id"], name: "index_test_types_on_user_id"
  end

  create_table "tests", force: :cascade do |t|
    t.datetime "date_received"
    t.datetime "date_done"
    t.integer  "test_type_id"
    t.integer  "medtech_id"
    t.integer  "order_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["medtech_id"], name: "index_tests_on_medtech_id"
    t.index ["order_id"], name: "index_tests_on_order_id"
    t.index ["test_type_id"], name: "index_tests_on_test_type_id"
    t.index ["user_id"], name: "index_tests_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",               default: "",              null: false
    t.string   "encrypted_password",     default: "",              null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.string   "first_name",             default: "First Name",    null: false
    t.string   "middle_name",            default: "Middle Name",   null: false
    t.string   "last_name",              default: "Last Name",     null: false
    t.integer  "role_id"
    t.integer  "user_id"
    t.bigint   "mobile_number",          default: 9171234567,      null: false
    t.string   "email",                  default: "abc@email.com", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,               null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
