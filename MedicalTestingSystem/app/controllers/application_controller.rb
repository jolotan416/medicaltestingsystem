class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :role_ids

  def role_ids
    @admin_id = Role.find_by_name("Administrator").id
    @employee_id = Role.find_by_name("Employee").id
  end
end
