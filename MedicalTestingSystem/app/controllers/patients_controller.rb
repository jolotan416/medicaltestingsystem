class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /patients
  # GET /patients.json
  def index
    @patients = Patient.all
  end

  def search
    if params[:searched_patient].nil? or params[:searched_patient] == ""
      redirect_to patients_path
      return
    end

    @patients = Patient.select("(first_name || ' ' ||  middle_name || ' ' || last_name) AS name, *").where("name LIKE '%" + params[:searched_patient] + "%'")
  end

  def add_patient_type
    @patient = Patient.find(params[:patient_id])
    @patient_lookup = PatientTypeLookup.new(:patient_id => params[:patient_id], :patient_type_id => params[:patient_type_id], :user_id => current_user.id)

    respond_to do |format|
      if @patient_lookup.save
        format.html { redirect_to @patient, notice: 'Patient was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_patient_type
    @patient_lookup = PatientTypeLookup.find(params[:patient_lookup])
    @patient = Patient.find(@patient_lookup.patient_id)
    @patient_lookup.destroy

    respond_to do |format|
      format.html { redirect_to @patient, notice: 'Discount was successfully removed.' }
      format.json { head :no_content }
    end
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @orders = Order.where("patient_id = " + @patient.id.to_s)
    @orders = @orders.sort_by{|order| order.date_ordered}
    @orders = @orders.reverse
    @tests = []
    @orders.each do |order|
      @tests.append(Test.where("order_id = " + order.id.to_s))
    end
    @patient_lookups = PatientTypeLookup.where("patient_id = " + @patient.id.to_s)
    @patient_types = []
    @patient_lookups.each do |patient_lookup|
      @patient_types.append(PatientType.find(patient_lookup.patient_type_id))
    end
    @all_patient_types = (PatientType.all - @patient_types).collect {|p| [p.name, p.id]}
  end

  # GET /patients/new
  def new
    @patient = Patient.new
    @birthday = ""
  end

  # GET /patients/1/edit
  def edit
    @birthday = @patient.birthday.strftime("%m/%d/%Y")
  end

  # POST /patients
  # POST /patients.json
  def create
    params[:patient][:user_id] = current_user.id
    split_birthday = params[:patient][:birthday].split("/")
    if split_birthday.count == 3
      params[:patient][:birthday] = (split_birthday[2] + "-" + split_birthday[0] + "-" + split_birthday[1]).to_date
    end
    @patient = Patient.new(patient_params)

    respond_to do |format|
      if @patient.save
        format.html { redirect_to @patient, notice: 'Patient was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    params[:patient][:user_id] = current_user.id
    split_birthday = params[:patient][:birthday].split("/")
    if split_birthday.count == 3
      params[:patient][:birthday] = (split_birthday[2] + "-" + split_birthday[0] + "-" + split_birthday[1]).to_date
    end
    respond_to do |format|
      if @patient.update(patient_params)
        format.html { redirect_to @patient, notice: 'Patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.require(:patient).permit(:first_name, :middle_name, :last_name, :sex, :birthday, :status, :address, :contact_no, :user_id)
    end
end
