class MedtechesController < ApplicationController
  before_action :set_medtech, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /medteches
  # GET /medteches.json
  def index
    @medteches = Medtech.all
    @physicians = Physician.all
  end

  # GET /medteches/1
  # GET /medteches/1.json
  def show
  end

  # GET /medteches/new
  def new
    @medtech = Medtech.new
  end

  # GET /medteches/1/edit
  def edit
  end

  # POST /medteches
  # POST /medteches.json
  def create
    params[:medtech][:user_id] = current_user.id
    @medtech = Medtech.new(medtech_params)

    respond_to do |format|
      if @medtech.save
        format.html { redirect_to personnels_path, notice: 'Medtech was successfully created.' }
        format.json { render :show, status: :created, location: @medtech }
      else
        format.html { render :new }
        format.json { render json: @medtech.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /medteches/1
  # PATCH/PUT /medteches/1.json
  def update
    params[:medtech][:user_id] = current_user.id
    respond_to do |format|
      if @medtech.update(medtech_params)
        format.html { redirect_to personnels_path, notice: 'Medtech was successfully updated.' }
        format.json { render :show, status: :ok, location: @medtech }
      else
        format.html { render :edit }
        format.json { render json: @medtech.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medteches/1
  # DELETE /medteches/1.json
  def destroy
    @medtech.destroy
    respond_to do |format|
      format.html { redirect_to personnels_url, notice: 'Medtech was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medtech
      @medtech = Medtech.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medtech_params
      params.require(:medtech).permit(:first_name, :middle_name, :last_name, :user_id)
    end
end
