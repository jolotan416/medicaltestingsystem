class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @patient_lookup = PatientTypeLookup.where("patient_id = " + @order.patient_id.to_s).collect {|p| [PatientType.find(p.patient_type_id).name, p.patient_type_id]}
    @tests = Test.where("order_id=" + @order.id.to_s)
    @physicians = []
    @tests.each do |t|
      physician = t.test_type.physician_id
      if physician.nil?
        @physicians.append("None")
      else
        @physicians.append(Physician.find(physician))
      end
    end
  end

  # GET /orders/new
  def new
    @patient = Patient.find(params[:patient_id])
    @order = Order.new(:user_id => current_user.id, :patient_id => params[:patient_id], :date_ordered => Time.now)
    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_discount
    @order = Order.find(params[:order_id])

    if params[:discount] == "-1"
      result = @order.update(:discount => 0)
    elsif params[:discount] != "0" && params[:discount] != ""
      result = @order.update(:discount => PatientType.find(params[:discount]).discount)
    else
      result = true
    end

    if result
      total = @order.subtotal - (@order.subtotal * @order.discount)
      if @order.payment == 0
        change = 0
      else
        change = @order.payment - total
      end
      result = @order.update(:total => total, :change => change)
    end

    respond_to do |format|
      if result
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def pay_order
    @order = Order.find(params[:order_id])

    result = @order.update(:payment => params[:payment])

    if result
      if @order.payment == 0
        change = 0
      else
        change = @order.payment - @order.total
      end
      result = @order.update(:change => change)
    end
    respond_to do |format|
      if result
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def print_order
    @order = Order.find(params[:order_id])
    @user = User.find(@order.user_id)
    @tests = Test.where("order_id=" + @order.id.to_s)
    @physicians = []
    @tests.each do |t|
      physician = t.test_type.physician_id
      if physician.nil?
        @physicians.append("None")
      else
        @physicians.append(Physician.find(physician))
      end
    end
    respond_to do |format|
      format.pdf do
        render pdf: @order.patient.first_name + " " + @order.patient.last_name  + " Order Form " + @order.date_ordered.localtime.strftime("%B-%d-%Y-%I-%M-%P")
      end
    end
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    params[:order][:date_ordered] = Time.now
    params[:order][:user_id] = current_user.id
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @patient = @order.patient
    @order.destroy
    respond_to do |format|
      format.html { redirect_to @patient, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:date_ordered, :subtotal, :discount, :total, :payment, :change, :patient_id, :user_id)
    end
end
