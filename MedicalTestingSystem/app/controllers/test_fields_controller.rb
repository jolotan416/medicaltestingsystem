class TestFieldsController < ApplicationController
  before_action :set_test_field, only: [:show, :edit, :update, :destroy]

  # GET /test_fields
  # GET /test_fields.json
  def index
    @test_fields = TestField.all
  end

  # GET /test_fields/1
  # GET /test_fields/1.json
  def show
  end

  # GET /test_fields/new
  def new
    @test_id = params[:test_id]
    @field_types = FieldType.all.collect { |field_type| [field_type.name, field_type.id] }
    @test_field = TestField.new
  end

  # GET /test_fields/1/edit
  def edit
    @test_id = @test_field.test_id
    @field_types = FieldType.all.collect { |field_type| [field_type.name, field_type.id] }
  end

  # POST /test_fields
  # POST /test_fields.json
  def create
    params[:test_field][:user_id] = current_user.id
    @test_field = TestField.new(test_field_params.permit(:field_type_id, :data, :user_id, :test_id))

    respond_to do |format|
      if @test_field.save
        format.html { redirect_to tests_show_results_path(:test_id => @test_field.test_id), notice: 'Test field was successfully created.' }
        format.json { render :show, status: :created, location: @test_field }
      else
        format.html { render :new }
        format.json { render json: @test_field.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_fields/1
  # PATCH/PUT /test_fields/1.json
  def update
    respond_to do |format|
      if @test_field.update(test_field_params.permit(:field_type_id, :data, :user_id, :test_id))
        format.html { redirect_to tests_show_results_path(:test_id => @test_field.test_id), notice: 'Test field was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_field }
      else
        format.html { render :edit }
        format.json { render json: @test_field.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_fields/1
  # DELETE /test_fields/1.json
  def destroy
    test_id = @test_field.test_id
    @test_field.destroy
    respond_to do |format|
      format.html { redirect_to tests_show_results_path(:test_id => test_id), notice: 'Test field was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_field
      @test_field = TestField.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_field_params
      params.fetch(:test_field, {})
    end
end
