class StaticController < ApplicationController
  before_action :authenticate_user!

  def index
    @lines = Line.all
    line_count = @lines.count

    if line_count == 1
      @line_count = "one"
    elsif line_count == 2
      @line_count = "two"
    elsif line_count == 3
      @line_count = "three"
    elsif line_count == 4
      @line_count = "four"
    elsif line_count == 5
      @line_count = "five"
    elsif line_count == 6
      @line_count = "six"
    elsif line_count == 7
      @line_count = "seven"
    elsif line_count == 8
      @line_count = "eight"
    elsif line_count == 9
      @line_count = "nine"
    elsif line_count == 10
      @line_count = "ten"
    end

    i = 0
    @tests = []
    @lines.each do |line|
      @tests.append([])
      test_types = TestType.where("line_id = " + line.id.to_s)
      test_types.each do |test_type|
        @tests[i] += Test.where("test_type_id = " + test_type.id.to_s + " and date_done is NULL")
      end
      @tests[i] = @tests[i].sort_by{|t| Order.find(t.order_id).date_ordered}
      i += 1
    end
  end
end
