class TestTypesController < ApplicationController
  before_action :set_test_type, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /test_types
  # GET /test_types.json
  def index
    @test_types = TestType.all
    @physicians = []
    @test_types.each do |test_type|
      physician = test_type.physician_id
      if physician.nil?
        @physicians.append("None")
      else
        @physicians.append(Physician.find(physician))
      end
    end
  end

  # GET /test_types/1
  # GET /test_types/1.json
  def show
    physician = @test_type.physician_id
    if physician.nil?
      @physician = "None"
    else
      @physician = Physician.find(physician)
    end
  end

  def search
    if params[:searched_test].nil? or params[:searched_test] == ""
      redirect_to test_types_path
      return
    end

    @test_types = TestType.where("name LIKE '%" + params[:searched_test] + "%'")
    @physicians = []
    @test_types.each do |test_type|
      physician = test_type.physician_id
      if physician.nil?
        @physicians.append("None")
      else
        @physicians.append(Physician.find(physician))
      end
    end
  end

  # GET /test_types/new
  def new
    @physicians = Physician.all.collect { |p| [p.first_name + " " + p.last_name, p.id] }.append(["None", 0])
    @lines = Line.all.collect { |l| [l.name, l.id] }
    @test_type = TestType.new
  end

  # GET /test_types/1/edit
  def edit
    @physicians = Physician.all.collect { |p| [p.first_name + " " + p.last_name, p.id] }.append(["None", 0])
    @lines = Line.all.collect { |l| [l.name, l.id] }
  end

  # POST /test_types
  # POST /test_types.json
  def create
    if params[:test_type][:physician_id] == '0'
      params[:test_type][:physician_id] = nil
    end
    params[:test_type][:user_id] = current_user.id
    @test_type = TestType.new(test_type_params)

    respond_to do |format|
      if @test_type.save
        format.html { redirect_to @test_type, notice: 'Test type was successfully created.' }
        format.json { render :show, status: :created, location: @test_type }
      else
        format.html { render :new }
        format.json { render json: @test_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_types/1
  # PATCH/PUT /test_types/1.json
  def update
    if params[:test_type][:physician_id] == '0'
      params[:test_type][:physician_id] = nil
    end
    params[:test_type][:user] = current_user.id

    respond_to do |format|
      if @test_type.update(test_type_params)
        format.html { redirect_to @test_type, notice: 'Test type was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_type }
      else
        format.html { render :edit }
        format.json { render json: @test_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_types/1
  # DELETE /test_types/1.json
  def destroy
    @test_type.destroy
    respond_to do |format|
      format.html { redirect_to test_types_url, notice: 'Test type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_type
      @test_type = TestType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_type_params
      params.require(:test_type).permit(:name, :amount, :physician_id, :line_id, :user_id)
    end
end
