class TestsController < ApplicationController
  before_action :set_test, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /tests
  # GET /tests.json
  def index
    @tests = Test.all
  end

  # GET /tests/1
  # GET /tests/1.json
  def show
    @order = @test.order
    if !@test.medtech_id.nil?
      @medtech = Medtech.find(@test.medtech_id)
    end
  end

  def show_results
    @t = Test.find(params[:test_id])
    test_type = TestType.find(@t.test_type_id)
    @test_type_name = test_type.name
    if !test_type.physician_id.nil?
      physician = Physician.find(test_type.physician_id.to_s)
      @test_type_name += " (" + physician.first_name + " " + physician.last_name +  ")"
    end
    patient = Patient.find(Order.find(@t.order_id).patient_id)
    @patient_name = patient.first_name + " " + patient.last_name
    @fields = TestField.where("test_id = " + @t.id.to_s)
  end

  def finish_test
    @test = Test.find(params[:test_id])
    @order = @test.order
    if @test.date_done.nil?
      params = {:date_done => Time.now}
    else
      params = {:date_done => nil}
    end
    respond_to do |format|
      if @test.update(params)
        format.html { redirect_to @order, notice: 'Test was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /tests/new
  def new
    all_test_types = TestType.all
    @test_types = []
    all_test_types.each do |test_type|
      info = []
      if test_type.physician_id.nil?
        info.append(test_type.name)
      else
        physician = Physician.find(test_type.physician_id)
        info.append(test_type.name + " (" + physician.first_name + " " + physician.last_name + ")")
      end
      info.append(test_type.id)
      @test_types.append(info)
    end

    @medteches = Medtech.all.collect {|m| [m.first_name + " " + m.last_name, m.id]}.append(["None", 0])

    @current_path = request.env['PATH_INFO']
    @order = Order.find(params[:order_id])
    @test = Test.new
  end

  # GET /tests/1/edit
  def edit
    all_test_types = TestType.all
    @test_types = []
    all_test_types.each do |test_type|
      info = []
      if test_type.physician_id.nil?
        info.append(test_type.name)
      else
        physician = Physician.find(test_type.physician_id)
        info.append(test_type.name + " (" + physician.first_name + " " + physician.last_name + ")")
      end
      info.append(test_type.id)
      @test_types.append(info)
    end
    @medteches = Medtech.all.collect {|m| [m.first_name + " " + m.last_name, m.id]}.append(["None", 0])

    @current_path = request.env['PATH_INFO']
    @order = Order.find(Test.find(params[:id]).order_id)
  end

  # POST /tests
  # POST /tests.json
  def create
    params[:test][:user_id] = current_user.id

    if params[:test][:medtech_id] == "0"
      params[:test][:medtech_id] = nil
    end

    @test = Test.new(test_params)
    @order = Order.find(params[:test][:order_id])
    @order.update(:subtotal => (@order.subtotal + @test.test_type.amount))
    @order.update(:total => (@order.subtotal - @order.subtotal * @order.discount))
    if @order.payment != 0
      @order.update(:change => (@order.payment - @order.total))
    end

    respond_to do |format|
      if @test.save
        format.html { redirect_to @order, notice: 'Test was successfully created.' }
        format.json { render :show, status: :created, location: @test }
      else
        format.html { redirect_to new_test_path(:order_id => params[:test][:order_id]) }
        format.json { render json: @test.errors, status: :unprocessable_entity }
        binding.pry
      end
    end
  end

  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    if params[:test][:medtech_id] == "0"
      params[:test][:medtech_id] = nil
    end

    if params[:test][:date_received] == "1"
      params[:test][:date_received] = Time.now
    else
      params[:test][:date_received] = nil
    end

    respond_to do |format|
      if @test.update(test_params)
        @tests = Test.where("order_id = " + @test.order_id.to_s)

        @subtotal = 0
        @tests.each do |t|
          @subtotal += t.test_type.amount
        end

        @order = Order.find(params[:test][:order_id])
        @order.update(:subtotal => @subtotal)
        @order.update(:total => (@order.subtotal - @order.subtotal * @order.discount))
        if @order.payment != 0
          @order.update(:change => (@order.payment - @order.total))
        end

        format.html { redirect_to @order, notice: 'Test was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @order = @test.order
    @test.destroy
    respond_to do |format|
      @tests = Test.where("order_id = " + @order.id.to_s)
      @subtotal = 0
      @tests.each do |t|
        @subtotal += t.test_type.amount
      end
      @order.update(:subtotal => @subtotal)
      @order.update(:total => (@order.subtotal - @order.subtotal * @order.discount))
      if @order.payment != 0
        @order.update(:change => (@order.payment - @order.total))
      end

      format.html { redirect_to @order, notice: 'Test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test
      @test = Test.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_params
      params.require(:test).permit(:date_received, :test_type_id, :medtech_id, :order_id, :user_id)
    end
end
