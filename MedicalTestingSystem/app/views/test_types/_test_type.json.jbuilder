json.extract! test_type, :id, :name, :amount, :physician_id, :line_id, :user_id, :created_at, :updated_at
json.url test_type_url(test_type, format: :json)