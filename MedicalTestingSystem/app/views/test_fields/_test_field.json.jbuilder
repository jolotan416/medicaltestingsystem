json.extract! test_field, :id, :created_at, :updated_at
json.url test_field_url(test_field, format: :json)