json.extract! patient_type, :id, :name, :discount, :user_id, :created_at, :updated_at
json.url patient_type_url(patient_type, format: :json)