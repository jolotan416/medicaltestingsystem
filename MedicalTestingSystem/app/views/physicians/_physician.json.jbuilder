json.extract! physician, :id, :first_name, :middle_name, :last_name, :prefix, :suffx, :user_id, :created_at, :updated_at
json.url physician_url(physician, format: :json)