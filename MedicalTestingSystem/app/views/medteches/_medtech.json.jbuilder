json.extract! medtech, :id, :first_name, :middle_name, :last_name, :user_id, :created_at, :updated_at
json.url medtech_url(medtech, format: :json)