json.extract! test, :id, :date_received, :test_type_id, :medtech_id, :order_id, :user_id, :created_at, :updated_at
json.url test_url(test, format: :json)