json.extract! order, :id, :date_ordered, :subtotal, :discount, :total, :payment, :change, :patient_id, :user_id, :created_at, :updated_at
json.url order_url(order, format: :json)