class Test < ApplicationRecord
  belongs_to :test_type
  belongs_to :order
  belongs_to :user
  has_many :test_fields, dependent: :destroy
end
