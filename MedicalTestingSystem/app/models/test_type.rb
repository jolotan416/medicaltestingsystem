class TestType < ApplicationRecord
  belongs_to :line
  belongs_to :user
  has_many :tests, dependent: :destroy
end
