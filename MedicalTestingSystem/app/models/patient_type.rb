class PatientType < ApplicationRecord
  belongs_to :user
  has_many :patient_type_lookups, dependent: :destroy
end
