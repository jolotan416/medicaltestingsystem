class PatientTypeLookup < ApplicationRecord
  belongs_to :patient
  belongs_to :patient_type
  belongs_to :user
end
