class FieldType < ApplicationRecord
  belongs_to :user
  has_many :test_fields, dependent: :destroy
end
