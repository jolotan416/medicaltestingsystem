class Patient < ApplicationRecord
  belongs_to :user
  has_many :patient_type_lookups, dependent: :destroy
  has_many :orders, dependent: :destroy
end
