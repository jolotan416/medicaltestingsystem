class Order < ApplicationRecord
  belongs_to :patient
  belongs_to :user
  has_many :tests, dependent: :destroy
end
