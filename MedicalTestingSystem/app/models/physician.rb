class Physician < ApplicationRecord
  belongs_to :user
  has_many :test_types, dependent: :destroy
end
