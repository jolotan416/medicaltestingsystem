require 'test_helper'

class MedtechesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medtech = medteches(:one)
  end

  test "should get index" do
    get personnels_url
    assert_response :success
  end

  test "should get new" do
    get new_medtech_url
    assert_response :success
  end

  test "should create medtech" do
    assert_difference('Medtech.count') do
      post personnels_url, params: { medtech: { first_name: @medtech.first_name, last_name: @medtech.last_name, middle_name: @medtech.middle_name, user_id: @medtech.user_id } }
    end

    assert_redirected_to medtech_url(Medtech.last)
  end

  test "should show medtech" do
    get medtech_url(@medtech)
    assert_response :success
  end

  test "should get edit" do
    get edit_medtech_url(@medtech)
    assert_response :success
  end

  test "should update medtech" do
    patch medtech_url(@medtech), params: { medtech: { first_name: @medtech.first_name, last_name: @medtech.last_name, middle_name: @medtech.middle_name, user_id: @medtech.user_id } }
    assert_redirected_to medtech_url(@medtech)
  end

  test "should destroy medtech" do
    assert_difference('Medtech.count', -1) do
      delete medtech_url(@medtech)
    end

    assert_redirected_to personnels_url
  end
end
