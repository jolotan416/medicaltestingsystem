require 'test_helper'

class TestFieldsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @test_field = test_fields(:one)
  end

  test "should get index" do
    get test_fields_url
    assert_response :success
  end

  test "should get new" do
    get new_test_field_url
    assert_response :success
  end

  test "should create test_field" do
    assert_difference('TestField.count') do
      post test_fields_url, params: { test_field: {  } }
    end

    assert_redirected_to test_field_url(TestField.last)
  end

  test "should show test_field" do
    get test_field_url(@test_field)
    assert_response :success
  end

  test "should get edit" do
    get edit_test_field_url(@test_field)
    assert_response :success
  end

  test "should update test_field" do
    patch test_field_url(@test_field), params: { test_field: {  } }
    assert_redirected_to test_field_url(@test_field)
  end

  test "should destroy test_field" do
    assert_difference('TestField.count', -1) do
      delete test_field_url(@test_field)
    end

    assert_redirected_to test_fields_url
  end
end
