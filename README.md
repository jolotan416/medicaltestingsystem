# ChoiceHealth System
## A System Designed for a Medical Testing Center with the following features:
### Admin:
1. Add/View/Edit/Delete Employees
2. Add/View/Edit/Delete Test Types

### Employees and Admins:
1. Add/View/Edit/Delete Patients
2. Add/Edit/Delete Doctor Schedule

### Regular Users, Employees, and Admins:
1. Login/Logout
2. Add/View/Edit/Delete User Orders
3. View Bill
4. View Order History
5. Edit User Profile
6. View Doctor Schedule
